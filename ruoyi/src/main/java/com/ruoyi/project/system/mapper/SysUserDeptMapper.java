package com.ruoyi.project.system.mapper;

import com.ruoyi.project.system.domain.SysUserDept;
import io.lettuce.core.dynamic.annotation.Param;

import java.util.List;

/**
 * 用户和部门关联Mapper接口
 *
 * @author ruoyi
 * @date 2023-01-06
 */
public interface SysUserDeptMapper {
    /**
     * 查询用户和部门关联
     *
     * @param userId 用户和部门关联ID
     * @return 用户和部门关联
     */
    SysUserDept selectSysUserDeptById(Long userId);

    /**
     * 查询用户和部门关联列表
     *
     * @param sysUserDept 用户和部门关联
     * @return 用户和部门关联集合
     */
    List<SysUserDept> selectSysUserDeptList(SysUserDept sysUserDept);

    /**
     * 新增用户和部门关联
     *
     * @param sysUserDept 用户和部门关联
     * @return 结果
     */
    int insertSysUserDept(SysUserDept sysUserDept);

    /**
     * 修改用户和部门关联
     *
     * @param sysUserDept 用户和部门关联
     * @return 结果
     */
    int updateSysUserDept(SysUserDept sysUserDept);

    /**
     * 删除用户和部门关联
     *
     * @param userId 用户和部门关联ID
     * @return 结果
     */
    int deleteSysUserDeptById(Long userId);

    /**
     * 批量删除用户和部门关联
     *
     * @param userIds 需要删除的数据ID
     * @return 结果
     */
    int deleteSysUserDeptByIds(Long[] userIds);

    /**
     * 获取当前用户不在的，其他部门的用户id列表
     * @param userId 用户id
     * @return 用户id列表
     */
    List<Long> selectUserIdsOtherDept(@Param("userId")Long userId);
}
