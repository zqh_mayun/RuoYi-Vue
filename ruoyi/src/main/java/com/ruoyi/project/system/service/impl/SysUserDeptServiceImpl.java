package com.ruoyi.project.system.service.impl;

import com.ruoyi.project.system.domain.SysUserDept;
import com.ruoyi.project.system.mapper.SysUserDeptMapper;
import com.ruoyi.project.system.service.ISysUserDeptService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户和部门关联Service业务层处理
 *
 * @author ruoyi
 * @date 2023-01-06
 */
@Service
public class SysUserDeptServiceImpl implements ISysUserDeptService {
    @Resource
    private SysUserDeptMapper sysUserDeptMapper;

    /**
     * 查询用户和部门关联
     *
     * @param userId 用户和部门关联ID
     * @return 用户和部门关联
     */
    @Override
    public SysUserDept selectSysUserDeptById(Long userId) {
        return sysUserDeptMapper.selectSysUserDeptById(userId);
    }

    /**
     * 查询用户和部门关联列表
     *
     * @param sysUserDept 用户和部门关联
     * @return 用户和部门关联
     */
    @Override
    public List<SysUserDept> selectSysUserDeptList(SysUserDept sysUserDept) {
        return sysUserDeptMapper.selectSysUserDeptList(sysUserDept);
    }

    /**
     * 新增用户和部门关联
     *
     * @param sysUserDept 用户和部门关联
     * @return 结果
     */
    @Override
    public int insertSysUserDept(SysUserDept sysUserDept) {
        return sysUserDeptMapper.insertSysUserDept(sysUserDept);
    }

    /**
     * 修改用户和部门关联
     *
     * @param sysUserDept 用户和部门关联
     * @return 结果
     */
    @Override
    public int updateSysUserDept(SysUserDept sysUserDept) {
        return sysUserDeptMapper.updateSysUserDept(sysUserDept);
    }

    /**
     * 批量删除用户和部门关联
     *
     * @param userIds 需要删除的用户和部门关联ID
     * @return 结果
     */
    @Override
    public int deleteSysUserDeptByIds(Long[] userIds) {
        return sysUserDeptMapper.deleteSysUserDeptByIds(userIds);
    }

    /**
     * 删除用户和部门关联信息
     *
     * @param userId 用户和部门关联ID
     * @return 结果
     */
    @Override
    public int deleteSysUserDeptById(Long userId) {
        return sysUserDeptMapper.deleteSysUserDeptById(userId);
    }

    @Override
    public List<Long> selectUserIdsOtherDept(Long userId) {
        return sysUserDeptMapper.selectUserIdsOtherDept(userId);
    }


}
