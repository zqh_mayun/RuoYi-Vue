package com.ruoyi.project.system.service;

import com.ruoyi.project.system.domain.SysUserDept;

import java.util.List;

/**
 * 用户和部门关联Service接口
 *
 * @author ruoyi
 * @date 2023-01-06
 */
public interface ISysUserDeptService {
    /**
     * 查询用户和部门关联
     *
     * @param userId 用户和部门关联ID
     * @return 用户和部门关联
     */
    SysUserDept selectSysUserDeptById(Long userId);

    /**
     * 查询用户和部门关联列表
     *
     * @param sysUserDept 用户和部门关联
     * @return 用户和部门关联集合
     */
    List<SysUserDept> selectSysUserDeptList(SysUserDept sysUserDept);

    /**
     * 新增用户和部门关联
     *
     * @param sysUserDept 用户和部门关联
     * @return 结果
     */
    int insertSysUserDept(SysUserDept sysUserDept);

    /**
     * 修改用户和部门关联
     *
     * @param sysUserDept 用户和部门关联
     * @return 结果
     */
    int updateSysUserDept(SysUserDept sysUserDept);

    /**
     * 批量删除用户和部门关联
     *
     * @param userIds 需要删除的用户和部门关联ID
     * @return 结果
     */
    int deleteSysUserDeptByIds(Long[] userIds);

    /**
     * 删除用户和部门关联信息
     *
     * @param userId 用户和部门关联ID
     * @return 结果
     */
    int deleteSysUserDeptById(Long userId);

    /**
     * 获取指定用户id的用户所在部门之外的其他用户的id数组
     * @param userId 用户id
     * @return SysUserDept列表
     */
    List<Long> selectUserIdsOtherDept(Long userId);
}
