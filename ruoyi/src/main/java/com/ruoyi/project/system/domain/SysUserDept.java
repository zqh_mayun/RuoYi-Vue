package com.ruoyi.project.system.domain;

import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 用户和部门关联对象 sys_user_dept
 *
 * @author ruoyi
 * @date 2023-01-06
 */
@Data
public class SysUserDept extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 部门ID
     */
    private Long deptId;
}
