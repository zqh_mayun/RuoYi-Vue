package com.ruoyi.project.system.controller;

import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.system.domain.SysUserDept;
import com.ruoyi.project.system.service.ISysUserDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户和部门关联Controller
 *
 * @author ruoyi
 * @date 2023-01-06
 */
@RestController
@RequestMapping("/system/user/dept")
public class SysUserDeptController extends BaseController {
    @Autowired
    private ISysUserDeptService sysUserDeptService;

    /**
     * 查询用户和部门关联列表
     */
    @GetMapping("/list")
    public TableDataInfo list(SysUserDept sysUserDept) {
        startPage();
        List<SysUserDept> list = sysUserDeptService.selectSysUserDeptList(sysUserDept);
        return getDataTable(list);
    }

    /**
     * 导出用户和部门关联列表
     */
    @Log(title = "用户和部门关联", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SysUserDept sysUserDept) {
        List<SysUserDept> list = sysUserDeptService.selectSysUserDeptList(sysUserDept);
        ExcelUtil<SysUserDept> util = new ExcelUtil<SysUserDept>(SysUserDept.class);
        return util.exportExcel(list, "dept");
    }

    /**
     * 获取用户和部门关联详细信息
     */
    @GetMapping(value = "/{userId}")
    public AjaxResult getInfo(@PathVariable("userId") Long userId) {
        return AjaxResult.success(sysUserDeptService.selectSysUserDeptById(userId));
    }

    /**
     * 新增用户和部门关联
     */
    @Log(title = "用户和部门关联", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysUserDept sysUserDept) {
        return toAjax(sysUserDeptService.insertSysUserDept(sysUserDept));
    }

    /**
     * 修改用户和部门关联
     */
    @Log(title = "用户和部门关联", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysUserDept sysUserDept) {
        return toAjax(sysUserDeptService.updateSysUserDept(sysUserDept));
    }

    /**
     * 删除用户和部门关联
     */
    @Log(title = "用户和部门关联", businessType = BusinessType.DELETE)
    @DeleteMapping("/{userIds}")
    public AjaxResult remove(@PathVariable Long[] userIds) {
        return toAjax(sysUserDeptService.deleteSysUserDeptByIds(userIds));
    }
}
