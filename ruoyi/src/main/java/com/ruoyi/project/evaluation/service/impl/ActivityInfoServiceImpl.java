package com.ruoyi.project.evaluation.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.project.evaluation.domain.ActivityInfo;
import com.ruoyi.project.evaluation.mapper.ActivityInfoMapper;
import com.ruoyi.project.evaluation.service.IActivityInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 部活动信息Service业务层处理
 *
 * @author ruoyi
 * @date 2023-01-06
 */
@Service
public class ActivityInfoServiceImpl implements IActivityInfoService {
    @Autowired
    private ActivityInfoMapper activityInfoMapper;

    /**
     * 查询部活动信息
     *
     * @param activityInfoId 部活动信息ID
     * @return 部活动信息
     */
    @Override
    public ActivityInfo selectActivityInfoById(Long activityInfoId) {
        return activityInfoMapper.selectActivityInfoById(activityInfoId);
    }

    /**
     * 查询部活动信息列表
     *
     * @param activityInfo 部活动信息
     * @return 部活动信息
     */
    @Override
    public List<ActivityInfo> selectActivityInfoList(ActivityInfo activityInfo) {
        return activityInfoMapper.selectActivityInfoList(activityInfo);
    }

    /**
     * 新增部活动信息
     *
     * @param activityInfo 部活动信息
     * @return 结果
     */
    @Override
    public int insertActivityInfo(ActivityInfo activityInfo) {
        activityInfo.setCreateTime(DateUtils.getNowDate());
        return activityInfoMapper.insertActivityInfo(activityInfo);
    }

    /**
     * 修改部活动信息
     *
     * @param activityInfo 部活动信息
     * @return 结果
     */
    @Override
    public int updateActivityInfo(ActivityInfo activityInfo) {
        activityInfo.setUpdateTime(DateUtils.getNowDate());
        return activityInfoMapper.updateActivityInfo(activityInfo);
    }

    /**
     * 批量删除部活动信息
     *
     * @param activityInfoIds 需要删除的部活动信息ID
     * @return 结果
     */
    @Override
    public int deleteActivityInfoByIds(Long[] activityInfoIds) {
        return activityInfoMapper.deleteActivityInfoByIds(activityInfoIds);
    }

    /**
     * 删除部活动信息信息
     *
     * @param activityInfoId 部活动信息ID
     * @return 结果
     */
    @Override
    public int deleteActivityInfoById(Long activityInfoId) {
        return activityInfoMapper.deleteActivityInfoById(activityInfoId);
    }
}
