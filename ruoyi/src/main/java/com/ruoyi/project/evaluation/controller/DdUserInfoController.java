package com.ruoyi.project.evaluation.controller;

import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.evaluation.domain.DdUserInfo;
import com.ruoyi.project.evaluation.service.IDdUserInfoService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 钉钉用户信息操作
 *
 * @author zqh
 * @date 2023-01-05
 */
@RestController
@RequestMapping("/system/info")
public class DdUserInfoController extends BaseController {
    @Resource
    private IDdUserInfoService ddUserInfoService;

    /**
     * 查询【请填写功能名称】列表
     */
    @GetMapping("/list")
    public TableDataInfo list(DdUserInfo ddUserInfo) {
        startPage();
        List<DdUserInfo> list = ddUserInfoService.selectDdUserInfoList(ddUserInfo);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DdUserInfo ddUserInfo) {
        List<DdUserInfo> list = ddUserInfoService.selectDdUserInfoList(ddUserInfo);
        ExcelUtil<DdUserInfo> util = new ExcelUtil<DdUserInfo>(DdUserInfo.class);
        return util.exportExcel(list, "info");
    }

    /**
     * 获取【请填写功能名称】详细信息
     */
    @GetMapping(value = "/{staffId}")
    public AjaxResult getInfo(@PathVariable("staffId") String staffId) {
        return AjaxResult.success(ddUserInfoService.selectDdUserInfoById(staffId));
    }

    /**
     * 新增【请填写功能名称】
     */
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DdUserInfo ddUserInfo) {
        return toAjax(ddUserInfoService.insertDdUserInfo(ddUserInfo));
    }

    /**
     * 修改【请填写功能名称】
     */
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DdUserInfo ddUserInfo) {
        return toAjax(ddUserInfoService.updateDdUserInfo(ddUserInfo));
    }

    /**
     * 删除【请填写功能名称】
     */
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @DeleteMapping("/{staffIds}")
    public AjaxResult remove(@PathVariable String[] staffIds) {
        return toAjax(ddUserInfoService.deleteDdUserInfoByIds(staffIds));
    }
}