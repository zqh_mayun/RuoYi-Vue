package com.ruoyi.project.evaluation.mapper;

import com.ruoyi.project.evaluation.domain.VoteInfo;

import java.util.List;

/**
 * 部门投票信息Mapper接口
 *
 * @author ruoyi
 * @date 2023-01-06
 */
public interface VoteInfoMapper {
    /**
     * 查询部门投票信息
     *
     * @param voteInfoId 部门投票信息ID
     * @return 部门投票信息
     */
    VoteInfo selectVoteInfoById(Long voteInfoId);

    /**
     * 查询部门投票信息列表
     *
     * @param voteInfo 部门投票信息
     * @return 部门投票信息集合
     */
    List<VoteInfo> selectVoteInfoList(VoteInfo voteInfo);

    /**
     * 新增部门投票信息
     *
     * @param voteInfo 部门投票信息
     * @return 结果
     */
    int insertVoteInfo(VoteInfo voteInfo);

    /**
     * 修改部门投票信息
     *
     * @param voteInfo 部门投票信息
     * @return 结果
     */
    int updateVoteInfo(VoteInfo voteInfo);

    /**
     * 删除部门投票信息
     *
     * @param voteInfoId 部门投票信息ID
     * @return 结果
     */
    int deleteVoteInfoById(Long voteInfoId);

    /**
     * 批量删除部门投票信息
     *
     * @param voteInfoIds 需要删除的数据ID
     * @return 结果
     */
    int deleteVoteInfoByIds(Long[] voteInfoIds);
}
