package com.ruoyi.project.evaluation.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.project.evaluation.domain.VoteInfo;
import com.ruoyi.project.evaluation.mapper.VoteInfoMapper;
import com.ruoyi.project.evaluation.service.IVoteInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 部门投票信息Service业务层处理
 *
 * @author ruoyi
 * @date 2023-01-06
 */
@Service
public class VoteInfoServiceImpl implements IVoteInfoService {
    @Autowired
    private VoteInfoMapper voteInfoMapper;

    /**
     * 查询部门投票信息
     *
     * @param voteInfoId 部门投票信息ID
     * @return 部门投票信息
     */
    @Override
    public VoteInfo selectVoteInfoById(Long voteInfoId) {
        return voteInfoMapper.selectVoteInfoById(voteInfoId);
    }

    /**
     * 查询部门投票信息列表
     *
     * @param voteInfo 部门投票信息
     * @return 部门投票信息
     */
    @Override
    public List<VoteInfo> selectVoteInfoList(VoteInfo voteInfo) {
        return voteInfoMapper.selectVoteInfoList(voteInfo);
    }

    /**
     * 新增部门投票信息
     *
     * @param voteInfo 部门投票信息
     * @return 结果
     */
    @Override
    public int insertVoteInfo(VoteInfo voteInfo) {
        voteInfo.setCreateTime(DateUtils.getNowDate());
        return voteInfoMapper.insertVoteInfo(voteInfo);
    }

    /**
     * 修改部门投票信息
     *
     * @param voteInfo 部门投票信息
     * @return 结果
     */
    @Override
    public int updateVoteInfo(VoteInfo voteInfo) {
        voteInfo.setUpdateTime(DateUtils.getNowDate());
        return voteInfoMapper.updateVoteInfo(voteInfo);
    }

    /**
     * 批量删除部门投票信息
     *
     * @param voteInfoIds 需要删除的部门投票信息ID
     * @return 结果
     */
    @Override
    public int deleteVoteInfoByIds(Long[] voteInfoIds) {
        return voteInfoMapper.deleteVoteInfoByIds(voteInfoIds);
    }

    /**
     * 删除部门投票信息信息
     *
     * @param voteInfoId 部门投票信息ID
     * @return 结果
     */
    @Override
    public int deleteVoteInfoById(Long voteInfoId) {
        return voteInfoMapper.deleteVoteInfoById(voteInfoId);
    }
}
