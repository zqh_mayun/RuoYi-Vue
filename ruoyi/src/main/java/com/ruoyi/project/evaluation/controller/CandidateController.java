package com.ruoyi.project.evaluation.controller;

import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.security.LoginUser;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.evaluation.domain.Candidate;
import com.ruoyi.project.evaluation.service.ICandidateService;
import com.ruoyi.project.system.domain.SysUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.catalina.security.SecurityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 参选人信息Controller
 *
 * @author ruoyi
 * @date 2023-01-06
 */
@RestController
@Api(tags = "候选人管理")
@RequestMapping("/system/candidate")
public class CandidateController extends BaseController {
    @Autowired
    private ICandidateService candidateService;

    /**
     * 查询参选人信息列表
     */
    @GetMapping("/list")
    public TableDataInfo list(Candidate candidate) {
        startPage();
        List<Candidate> list = candidateService.selectCandidateList(candidate);
        return getDataTable(list);
    }

    /**
     * 导出参选人信息列表
     */
    @Log(title = "导出参选人信息列表", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Candidate candidate) {
        List<Candidate> list = candidateService.selectCandidateList(candidate);
        ExcelUtil<Candidate> util = new ExcelUtil<Candidate>(Candidate.class);
        return util.exportExcel(list, "candidate");
    }

    /**
     * 获取参选人信息详细信息
     */
    @GetMapping(value = "/{name}")
    public AjaxResult getInfo(@PathVariable("name") String name) {
        return AjaxResult.success(candidateService.selectCandidateById(name));
    }

    /**
     * 新增参选人信息
     */
    @Log(title = "新增参选人信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Candidate candidate) {
        return toAjax(candidateService.insertCandidate(candidate));
    }

    /**
     * 修改参选人信息
     */
    @Log(title = "修改参选人信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Candidate candidate) {
        return toAjax(candidateService.updateCandidate(candidate));
    }

    /**
     * 删除参选人信息
     */
    @Log(title = "删除参选人信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{names}")
    public AjaxResult remove(@PathVariable String[] names) {
        return toAjax(candidateService.deleteCandidateByIds(names));
    }

    /**
     * 查询非本部门候选人信息
     */
    @GetMapping("/selectOtherDept")
    @ApiOperation("查询非本部门候选人信息")
    public AjaxResult selectOtherDept() {
        return AjaxResult.success("操作成功", candidateService.selectOtherDeptCandidate());
    }
}
