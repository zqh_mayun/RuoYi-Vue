package com.ruoyi.project.evaluation.service.impl;

import com.ruoyi.project.evaluation.domain.DdUserInfo;
import com.ruoyi.project.evaluation.mapper.DdUserInfoMapper;
import com.ruoyi.project.evaluation.service.IDdUserInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 【请填写功能名称】Service业务层处理
 *
 * @author zqh
 * @date 2023-01-05
 */
@Service
public class DdUserInfoServiceImpl implements IDdUserInfoService {
    @Resource
    private DdUserInfoMapper ddUserInfoMapper;

    /**
     * 查询【请填写功能名称】
     *
     * @param staffId 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public DdUserInfo selectDdUserInfoById(String staffId) {
        return ddUserInfoMapper.selectDdUserInfoById(staffId);
    }

    /**
     * 查询【请填写功能名称】列表
     *
     * @param ddUserInfo 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<DdUserInfo> selectDdUserInfoList(DdUserInfo ddUserInfo) {
        return ddUserInfoMapper.selectDdUserInfoList(ddUserInfo);
    }

    /**
     * 新增【请填写功能名称】
     *
     * @param ddUserInfo 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertDdUserInfo(DdUserInfo ddUserInfo) {
        return ddUserInfoMapper.insertDdUserInfo(ddUserInfo);
    }

    /**
     * 修改【请填写功能名称】
     *
     * @param ddUserInfo 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateDdUserInfo(DdUserInfo ddUserInfo) {
        return ddUserInfoMapper.updateDdUserInfo(ddUserInfo);
    }

    /**
     * 批量删除【请填写功能名称】
     *
     * @param staffIds 需要删除的【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteDdUserInfoByIds(String[] staffIds) {
        return ddUserInfoMapper.deleteDdUserInfoByIds(staffIds);
    }

    /**
     * 删除【请填写功能名称】信息
     *
     * @param staffId 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteDdUserInfoById(String staffId) {
        return ddUserInfoMapper.deleteDdUserInfoById(staffId);
    }
}