package com.ruoyi.project.evaluation.controller;

import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.evaluation.domain.ActivityInfo;
import com.ruoyi.project.evaluation.service.IActivityInfoService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 部活动信息Controller
 *
 * @author ruoyi
 * @date 2023-01-06
 */
@RestController
@RequestMapping("/system/activity")
public class ActivityInfoController extends BaseController {
    @Resource
    private IActivityInfoService activityInfoService;

    /**
     * 查询部活动信息列表
     */
    @GetMapping("/list")
    public TableDataInfo list(ActivityInfo activityInfo) {
        startPage();
        List<ActivityInfo> list = activityInfoService.selectActivityInfoList(activityInfo);
        return getDataTable(list);
    }

    /**
     * 导出部活动信息列表
     */
    @Log(title = "部活动信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ActivityInfo activityInfo) {
        List<ActivityInfo> list = activityInfoService.selectActivityInfoList(activityInfo);
        ExcelUtil<ActivityInfo> util = new ExcelUtil<>(ActivityInfo.class);
        return util.exportExcel(list, "info");
    }

    /**
     * 获取部活动信息详细信息
     */
    @GetMapping(value = "/{activityInfoId}")
    public AjaxResult getInfo(@PathVariable("activityInfoId") Long activityInfoId) {
        return AjaxResult.success(activityInfoService.selectActivityInfoById(activityInfoId));
    }

    /**
     * 新增部活动信息
     */
    @Log(title = "部活动信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ActivityInfo activityInfo) {
        return toAjax(activityInfoService.insertActivityInfo(activityInfo));
    }

    /**
     * 修改部活动信息
     */
    @Log(title = "部活动信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ActivityInfo activityInfo) {
        return toAjax(activityInfoService.updateActivityInfo(activityInfo));
    }

    /**
     * 删除部活动信息
     */
    @Log(title = "部活动信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{activityInfoIds}")
    public AjaxResult remove(@PathVariable Long[] activityInfoIds) {
        return toAjax(activityInfoService.deleteActivityInfoByIds(activityInfoIds));
    }
}
