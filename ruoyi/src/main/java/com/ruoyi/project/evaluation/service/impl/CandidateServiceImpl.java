package com.ruoyi.project.evaluation.service.impl;

import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.project.evaluation.domain.Candidate;
import com.ruoyi.project.evaluation.mapper.CandidateMapper;
import com.ruoyi.project.evaluation.service.ICandidateService;
import com.ruoyi.project.system.domain.SysUser;
import com.ruoyi.project.system.service.ISysUserDeptService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

/**
 * 参选人信息Service业务层处理
 *
 * @author ruoyi
 * @date 2023-01-06
 */
@Service
public class CandidateServiceImpl implements ICandidateService {
    @Resource
    private CandidateMapper candidateMapper;

    @Resource
    private ISysUserDeptService sysUserDeptService;

    /**
     * 查询参选人信息
     *
     * @param name 参选人信息ID
     * @return 参选人信息
     */
    @Override
    public Candidate selectCandidateById(String name) {
        return candidateMapper.selectCandidateById(name);
    }

    /**
     * 查询参选人信息列表
     *
     * @param candidate 参选人信息
     * @return 参选人信息
     */
    @Override
    public List<Candidate> selectCandidateList(Candidate candidate) {
        return candidateMapper.selectCandidateList(candidate);
    }

    /**
     * 新增参选人信息
     *
     * @param candidate 参选人信息
     * @return 结果
     */
    @Override
    public int insertCandidate(Candidate candidate) {
        return candidateMapper.insertCandidate(candidate);
    }

    /**
     * 修改参选人信息
     *
     * @param candidate 参选人信息
     * @return 结果
     */
    @Override
    public int updateCandidate(Candidate candidate) {
        return candidateMapper.updateCandidate(candidate);
    }

    /**
     * 批量删除参选人信息
     *
     * @param names 需要删除的参选人信息ID
     * @return 结果
     */
    @Override
    public int deleteCandidateByIds(String[] names) {
        return candidateMapper.deleteCandidateByIds(names);
    }

    /**
     * 删除参选人信息信息
     *
     * @param name 参选人信息ID
     * @return 结果
     */
    @Override
    public int deleteCandidateById(String name) {
        return candidateMapper.deleteCandidateById(name);
    }

    @Override
    public List<Candidate> selectOtherDeptCandidate() {
        // 获取用户信息
        SysUser currentUser = SecurityUtils.getLoginUser().getUser();
        List<Long> userIds = sysUserDeptService.selectUserIdsOtherDept(currentUser.getUserId());
        // 根据用户id获取候选人用户信息
        List<Candidate> candidates = candidateMapper.selectCandidateByUserIds(userIds);
        // 打乱数组
        Collections.shuffle(candidates);
        return candidates;
    }


}
