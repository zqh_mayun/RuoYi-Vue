package com.ruoyi.project.evaluation.service;

import com.ruoyi.project.evaluation.domain.Candidate;

import java.util.List;

/**
 * 参选人信息Service接口
 *
 * @author ruoyi
 * @date 2023-01-06
 */
public interface ICandidateService {
    /**
     * 查询参选人信息
     *
     * @param name 参选人信息ID
     * @return 参选人信息
     */
    Candidate selectCandidateById(String name);

    /**
     * 查询参选人信息列表
     *
     * @param candidate 参选人信息
     * @return 参选人信息集合
     */
    List<Candidate> selectCandidateList(Candidate candidate);

    /**
     * 新增参选人信息
     *
     * @param candidate 参选人信息
     * @return 结果
     */
    int insertCandidate(Candidate candidate);

    /**
     * 修改参选人信息
     *
     * @param candidate 参选人信息
     * @return 结果
     */
    int updateCandidate(Candidate candidate);

    /**
     * 批量删除参选人信息
     *
     * @param names 需要删除的参选人信息ID
     * @return 结果
     */
    int deleteCandidateByIds(String[] names);

    /**
     * 删除参选人信息信息
     *
     * @param name 参选人信息ID
     * @return 结果
     */
    int deleteCandidateById(String name);

    /**
     * 获取除当前用户所在部门外的其他部门的候选人，并且打乱数组顺序
     * @return
     */
    List<Candidate> selectOtherDeptCandidate();
}
