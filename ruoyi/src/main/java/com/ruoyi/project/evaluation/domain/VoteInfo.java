package com.ruoyi.project.evaluation.domain;

import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 部门投票信息对象 vote_info
 *
 * @author ruoyi
 * @date 2023-01-06
 */
public class VoteInfo extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 投票信息ID
     */
    private Long voteInfoId;

    /**
     * 活动信息ID
     */
    @Excel(name = "活动信息ID")
    private Long activityInfoId;

    /**
     * 投票次数
     */
    @Excel(name = "投票次数")
    private Long count;

    /**
     * 部门id
     */
    @Excel(name = "部门id")
    private Long deptId;

    /**
     * 候选人id
     */
    @Excel(name = "候选人id")
    private Long candidateId;


    public void setVoteInfoId(Long voteInfoId) {
        this.voteInfoId = voteInfoId;
    }

    public Long getVoteInfoId() {
        return voteInfoId;
    }

    public void setActivityInfoId(Long activityInfoId) {
        this.activityInfoId = activityInfoId;
    }

    public Long getActivityInfoId() {
        return activityInfoId;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Long getCount() {
        return count;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setCandidateId(Long candidateId) {
        this.candidateId = candidateId;
    }

    public Long getCandidateId() {
        return candidateId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("voteInfoId", getVoteInfoId())
                .append("activityInfoId", getActivityInfoId())
                .append("count", getCount())
                .append("deptId", getDeptId())
                .append("candidateId", getCandidateId())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .toString();
    }
}
