package com.ruoyi.project.evaluation.mapper;

import com.ruoyi.project.evaluation.domain.Candidate;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 参选人信息Mapper接口
 *
 * @author ruoyi
 * @date 2023-01-06
 */
public interface CandidateMapper {
    /**
     * 查询参选人信息
     *
     * @param name 参选人信息ID
     * @return 参选人信息
     */
    Candidate selectCandidateById(String name);

    /**
     * 查询参选人信息列表
     *
     * @param candidate 参选人信息
     * @return 参选人信息集合
     */
    List<Candidate> selectCandidateList(Candidate candidate);

    /**
     * 根据用户id列表获取候选人信息
     * @param userIds 用户id
     * @return 候选人信息
     */
    List<Candidate> selectCandidateByUserIds(@Param("userIds") List<Long> userIds);

    /**
     * 新增参选人信息
     *
     * @param candidate 参选人信息
     * @return 结果
     */
    int insertCandidate(Candidate candidate);

    /**
     * 修改参选人信息
     *
     * @param candidate 参选人信息
     * @return 结果
     */
    int updateCandidate(Candidate candidate);

    /**
     * 删除参选人信息
     *
     * @param name 参选人信息ID
     * @return 结果
     */
    int deleteCandidateById(String name);

    /**
     * 批量删除参选人信息
     *
     * @param names 需要删除的数据ID
     * @return 结果
     */
    int deleteCandidateByIds(String[] names);
}
