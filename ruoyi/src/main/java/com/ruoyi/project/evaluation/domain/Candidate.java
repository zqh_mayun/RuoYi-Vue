package com.ruoyi.project.evaluation.domain;

import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 参选人信息对象 candidate
 *
 * @author ruoyi
 * @date 2023-01-06
 */
@Data
public class Candidate extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 姓名
     */
    @Excel(name = "姓名")
    private String name;

    /**
     * 手机号码
     */
    @Excel(name = "手机号码")
    private String mobile;

    /**
     * 用户id
     */
    @Excel(name = "用户id")
    private Long userId;
}
