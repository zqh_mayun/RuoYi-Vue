package com.ruoyi.project.evaluation.controller;

import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.evaluation.domain.VoteInfo;
import com.ruoyi.project.evaluation.service.IVoteInfoService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 部门投票信息Controller
 *
 * @author ruoyi
 * @date 2023-01-06
 */
@RestController
@RequestMapping("/system/vote")
public class VoteInfoController extends BaseController {
    @Resource
    private IVoteInfoService voteInfoService;

    /**
     * 查询部门投票信息列表
     */
    @GetMapping("/list")
    public TableDataInfo list(VoteInfo voteInfo) {
        startPage();
        List<VoteInfo> list = voteInfoService.selectVoteInfoList(voteInfo);
        return getDataTable(list);
    }

    /**
     * 导出部门投票信息列表
     */
    @Log(title = "部门投票信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(VoteInfo voteInfo) {
        List<VoteInfo> list = voteInfoService.selectVoteInfoList(voteInfo);
        ExcelUtil<VoteInfo> util = new ExcelUtil<VoteInfo>(VoteInfo.class);
        return util.exportExcel(list, "info");
    }

    /**
     * 获取部门投票信息详细信息
     */
    @GetMapping(value = "/{voteInfoId}")
    public AjaxResult getInfo(@PathVariable("voteInfoId") Long voteInfoId) {
        return AjaxResult.success(voteInfoService.selectVoteInfoById(voteInfoId));
    }

    /**
     * 新增部门投票信息
     */
    @Log(title = "部门投票信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody VoteInfo voteInfo) {
        return toAjax(voteInfoService.insertVoteInfo(voteInfo));
    }

    /**
     * 修改部门投票信息
     */
    @Log(title = "部门投票信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody VoteInfo voteInfo) {
        return toAjax(voteInfoService.updateVoteInfo(voteInfo));
    }

    /**
     * 删除部门投票信息
     */
    @Log(title = "部门投票信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{voteInfoIds}")
    public AjaxResult remove(@PathVariable Long[] voteInfoIds) {
        return toAjax(voteInfoService.deleteVoteInfoByIds(voteInfoIds));
    }
}
