package com.ruoyi.project.evaluation.service;

import com.ruoyi.project.evaluation.domain.DdUserInfo;

import java.util.List;

/**
 * 【请填写功能名称】Service接口
 *
 * @author zqh
 * @date 2023-01-05
 */
public interface IDdUserInfoService {
    /**
     * 查询【请填写功能名称】
     *
     * @param staffId 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    DdUserInfo selectDdUserInfoById(String staffId);

    /**
     * 查询【请填写功能名称】列表
     *
     * @param ddUserInfo 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    List<DdUserInfo> selectDdUserInfoList(DdUserInfo ddUserInfo);

    /**
     * 新增【请填写功能名称】
     *
     * @param ddUserInfo 【请填写功能名称】
     * @return 结果
     */
    int insertDdUserInfo(DdUserInfo ddUserInfo);

    /**
     * 修改【请填写功能名称】
     *
     * @param ddUserInfo 【请填写功能名称】
     * @return 结果
     */
    int updateDdUserInfo(DdUserInfo ddUserInfo);

    /**
     * 批量删除【请填写功能名称】
     *
     * @param staffIds 需要删除的【请填写功能名称】ID
     * @return 结果
     */
    int deleteDdUserInfoByIds(String[] staffIds);

    /**
     * 删除【请填写功能名称】信息
     *
     * @param staffId 【请填写功能名称】ID
     * @return 结果
     */
    int deleteDdUserInfoById(String staffId);
}