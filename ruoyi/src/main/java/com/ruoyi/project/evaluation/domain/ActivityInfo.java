package com.ruoyi.project.evaluation.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * 活动信息对象 activity_info
 *
 * @author ruoyi
 * @date 2023-01-06
 */
@Data
public class ActivityInfo extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 活动信息ID
     */
    private Long activityInfoId;

    /**
     * 开始时间
     */
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date voteStartTime;

    /**
     * 结束时间
     */
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date voteEndTime;

    /**
     * 每个部门允许投票次数
     */
    @Excel(name = "每个部门允许投票次数")
    private Long count;

}