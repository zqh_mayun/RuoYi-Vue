package com.ruoyi.project.evaluation.service;

import com.ruoyi.project.evaluation.domain.ActivityInfo;

import java.util.List;

/**
 * 部活动信息Service接口
 *
 * @author ruoyi
 * @date 2023-01-06
 */
public interface IActivityInfoService {
    /**
     * 查询部活动信息
     *
     * @param activityInfoId 部活动信息ID
     * @return 部活动信息
     */
    ActivityInfo selectActivityInfoById(Long activityInfoId);

    /**
     * 查询部活动信息列表
     *
     * @param activityInfo 部活动信息
     * @return 部活动信息集合
     */
    List<ActivityInfo> selectActivityInfoList(ActivityInfo activityInfo);

    /**
     * 新增部活动信息
     *
     * @param activityInfo 部活动信息
     * @return 结果
     */
    int insertActivityInfo(ActivityInfo activityInfo);

    /**
     * 修改部活动信息
     *
     * @param activityInfo 部活动信息
     * @return 结果
     */
    int updateActivityInfo(ActivityInfo activityInfo);

    /**
     * 批量删除部活动信息
     *
     * @param activityInfoIds 需要删除的部活动信息ID
     * @return 结果
     */
    int deleteActivityInfoByIds(Long[] activityInfoIds);

    /**
     * 删除部活动信息信息
     *
     * @param activityInfoId 部活动信息ID
     * @return 结果
     */
    int deleteActivityInfoById(Long activityInfoId);
}
