package com.ruoyi.project.evaluation.domain;


import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 【请填写功能名称】对象 dd_user_info
 *
 * @author zqh
 * @date 2023-01-05
 */
@Data
public class DdUserInfo extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 钉钉员工id
     */
    @Excel(name = "钉钉员工id")
    private String staffId;

    /**
     * 员工姓名
     */
    @Excel(name = "员工姓名")
    private String name;

    /**
     * 手机号码
     */
    @Excel(name = "手机号码")
    private String mobile;

    /**
     * 部门
     */
    @Excel(name = "部门")
    private String deptName;

    /**
     * 职位
     */
    @Excel(name = "职位")
    private String title;

    /**
     * 工号
     */
    @Excel(name = "工号")
    private String jobNumber;

    /**
     * 是否主管(是/否)
     */
    @Excel(name = "是否主管(是/否)")
    private String isDeptManageList;

    /**
     * 直属主管UserID（staff_id）
     */
    @Excel(name = "直属主管UserID", readConverterExp = "s=taff_id")
    private String manageStaffId;

    /**
     * 邮箱
     */
    @Excel(name = "邮箱")
    private String email;

    /**
     * 分机号
     */
    @Excel(name = "分机号")
    private String extNumber;

    /**
     * 办公地点
     */
    @Excel(name = "办公地点")
    private String workStation;

    /**
     * 入职时间
     */
    @Excel(name = "入职时间")
    private String hiredDate;

    /**
     * 激活状态
     */
    @Excel(name = "激活状态")
    private String active;

    /**
     * 归属组织
     */
    @Excel(name = "归属组织")
    private String exclusiveOrgName;

    /**
     * 登录id
     */
    @Excel(name = "登录id")
    private String loginId;

    /**
     * 初始密码
     */
    @Excel(name = "初始密码")
    private String initPassword;

    /**
     * 角色
     */
    @Excel(name = "角色")
    private String lableJson;
}
