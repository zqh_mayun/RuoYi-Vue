import com.ruoyi.RuoYiApplication;
import com.ruoyi.project.evaluation.domain.DdUserInfo;
import com.ruoyi.project.evaluation.mapper.DdUserInfoMapper;
import com.ruoyi.project.system.domain.SysDept;
import com.ruoyi.project.system.domain.SysUser;
import com.ruoyi.project.system.domain.SysUserDept;
import com.ruoyi.project.system.domain.SysUserRole;
import com.ruoyi.project.system.mapper.SysDeptMapper;
import com.ruoyi.project.system.mapper.SysUserDeptMapper;
import com.ruoyi.project.system.mapper.SysUserMapper;
import com.ruoyi.project.system.mapper.SysUserRoleMapper;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.*;

@SpringBootTest(classes = RuoYiApplication.class)
@RunWith(SpringRunner.class)
public class RuoYiApplicationTest {

    @Resource
    private DdUserInfoMapper ddUserInfoMapper;

    @Resource
    private SysUserMapper sysUserMapper;

    @Resource
    private SysDeptMapper sysDeptMapper;

    @Resource
    private SysUserDeptMapper sysUserDeptMapper;

    @Resource
    private SysUserRoleMapper sysUserRoleMapper;

    @Test
    public void testGetSysDept() {
        SysDept sysDept = sysDeptMapper.selectDeptById(1L);
        System.out.println(sysDept.toString());

        SysUserRole sysUserRole = sysUserRoleMapper.selectUserRoleByUserIdAndRoleId(312L, 100L);
        System.out.println(sysUserRole.toString());
    }


    /**
     * 1、根据钉钉用户信息：初始化用户信息、部门信息
     * 2、初始化需要参选明星员工的人员信息：和 用户信息关联上
     */
    @Test
    public void initData() {
        // 获取所有用户信息
        List<DdUserInfo> ddUserInfos = ddUserInfoMapper.selectDdUserInfoList(new DdUserInfo());

        Assert.assertFalse(CollectionUtils.isEmpty(ddUserInfos));

        Map<String, SysDept> deptmaps = new HashMap<>();
        // 存入根部门
        String rootPath = "重庆监督检测中心";
        deptmaps.put(rootPath, sysDeptMapper.selectDeptById(1L));

        // 1、创建或更新部门信息,根据部门完整路径更新部门信息
        ddUserInfos.forEach(d -> {
            if (StringUtils.hasText(d.getDeptName())) {
                String[] allDepts = d.getDeptName().split(",");

                if (!CollectionUtils.isEmpty(Arrays.asList(allDepts))) {
                    int count = 0;
                    // 部门管理员信息，跟之前的部门信息先后对应
                    String[] deptManage = d.getIsDeptManageList().split(",");
                    for (String deptPath : allDepts) {
                        String currentPath = rootPath;

                        if (deptPath.contains("茶园主场所-主场所-市场部")) {
                            System.out.println();
                        }

                        String[] deptNames = deptPath.split("-");
                        if (!CollectionUtils.isEmpty(Arrays.asList(deptNames))) {
                            SysDept parentDept = null;
                            for (int i = 0; i < deptNames.length; i++) {
                                String name = deptNames[i];

                                currentPath = currentPath + "-" + name;

                                if (StringUtils.hasText(name)) {
                                    SysDept sysDept = new SysDept();
                                    sysDept.setDeptNameBack(name.trim());
                                    // 根据名称查询部门信息如果存在则更新
                                    sysDept.setLevel((long) (i + 1));
                                    List<SysDept> sysDepts = sysDeptMapper.selectDeptListByDeptPath(currentPath, false);

                                    if (i == 0) {
                                        parentDept = deptmaps.get(rootPath);
                                    }


                                    if (CollectionUtils.isEmpty(sysDepts)) {
                                        // 设置ancessor
                                        sysDept.setAncestors(parentDept.getAncestors() == null ? "" : parentDept.getAncestors() + "," + parentDept.getDeptId());
                                        sysDept.setParentId(parentDept.getDeptId());
                                        sysDept.setDeptName(name);
                                        sysDept.setDeptPath(parentDept.getDeptPath() + "-" + name);

                                        sysDept.setCreateBy("系统创建");
                                        sysDept.setCreateTime(new Date());

                                        // 如果是路径的最后一个部门则添加是否是管理员信息
                                        if (i == deptNames.length - 1 && "是".equals(deptManage[count])) {
                                            sysDept.setLeader(d.getName());
                                            sysDept.setPhone(d.getMobile());
                                            sysDept.setEmail(d.getEmail());
                                        }

                                        sysDeptMapper.insertDept(sysDept);

                                        List<SysDept> newDeptList = sysDeptMapper.selectDeptList(sysDept);
                                        sysDept = newDeptList.get(0);
                                    } else {
                                        sysDept = sysDepts.get(0);

                                        // 设置ancessor
                                        sysDept.setAncestors(parentDept.getAncestors() == null ? "" : parentDept.getAncestors() + "," + parentDept.getDeptId());
                                        sysDept.setParentId(parentDept.getDeptId());
                                        sysDept.setDeptName(name);
                                        sysDept.setDeptPath(parentDept.getDeptPath() + "-" + name);

                                        // 判断是否需要更新管理员信息,如果是路径的最后一个部门则添加是否是管理员信息
                                        if (i == deptNames.length - 1 && "是".equals(deptManage[count])) {
                                            sysDept.setLeader(d.getName());
                                            sysDept.setPhone(d.getMobile());
                                            sysDept.setEmail(d.getEmail());
                                        }
                                        sysDept.setUpdateBy("系统更新");
                                        sysDept.setUpdateTime(new Date());

                                        sysDeptMapper.updateDept(sysDept);
                                    }
                                    deptmaps.put(sysDept.getDeptPath(), sysDept);
                                    parentDept = sysDept;
                                }
                            }
                        }

                        // 先测试，一个人
                        count++;
                    }
                }
            }
        });

        // 2、初始化用户信息，更新用户所在部门 TODO
        ddUserInfos.parallelStream().forEach(d -> {
            SysUser user = new SysUser();
            user.setPhonenumber(d.getMobile());

            // 根据手机号码查询系统用户信息
            List<SysUser> sysUsers = sysUserMapper.selectUserList(user);
            // 设置账户名称为【手机号码】
            user.setUserName(d.getMobile());
            // 设置初始密码为【12345678】
            user.setPassword(d.getInitPassword());

            user.setNickName(d.getName());
            user.setUserType("01");
            user.setEmail(d.getEmail());
            // 性别设置为未知
            user.setSex("2");
            user.setStatus("0");
            user.setDelFlag("0");
            Date date = new Date();
            // b不存在用户则新增用户，否则更新用户信息
            if (CollectionUtils.isEmpty(sysUsers)) {
                user.setCreateBy("系统创建");
                user.setCreateTime(date);
                sysUserMapper.insertUser(user);
            } else {
                user.setUserId(sysUsers.get(0).getUserId());

                // 更新用户所在的部门
                String[] allDepts = d.getDeptName().split(",");
                StringBuilder depts = new StringBuilder();
                if (!CollectionUtils.isEmpty(Arrays.asList(allDepts))) {
                    for (String allDept : allDepts) {
                        SysDept dept = deptmaps.get(rootPath + "-" + allDept);
                        if (dept != null) {
                            SysUserDept userDept = new SysUserDept();
                            userDept.setUserId(user.getUserId());
                            userDept.setDeptId(dept.getDeptId());
                            List<SysUserDept> sysUserDepts = sysUserDeptMapper.selectSysUserDeptList(userDept);
                            // 为空则插入关系
                            if (CollectionUtils.isEmpty(sysUserDepts)) {
                                sysUserDeptMapper.insertSysUserDept(userDept);
                            }
                        }
                    }
                }

                // 更新用户的默认角色： 普通员工角色 100
                SysUserRole sysUserRole = sysUserRoleMapper.selectUserRoleByUserIdAndRoleId(user.getUserId(), 100L);
                if (user.getUserId() == 312){
                    System.out.println();
                }
                // 没有这个角色则新增
                if (sysUserRole == null) {
                    List<SysUserRole> sysUserRoleList = new ArrayList<>(1);
                    SysUserRole sysUserRole1 = new SysUserRole();
                    sysUserRole1.setUserId(user.getUserId());
                    sysUserRole1.setRoleId(100L);
                    sysUserRoleList.add(sysUserRole1);

                    sysUserRoleMapper.batchUserRole(sysUserRoleList);
                }

                user.setUpdateBy("系统更新");
                user.setUpdateTime(date);
                sysUserMapper.updateUser(user);
            }


        });


        // 初始化参选明星员工的人员信息

    }
}
