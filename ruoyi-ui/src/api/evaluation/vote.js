import request from '@/utils/request'

// 查询非本部门候选人信息
export function selectOtherDept() {
    return request({
        url: '/system/candidate/selectOtherDept',
        method: 'get'
    })
}
