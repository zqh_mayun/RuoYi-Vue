import request from '@/utils/request'

// 查询参选人信息列表
export function listCandidate(query) {
    return request({
        url: '/system/candidate/list',
        method: 'get',
        params: query
    })
}

// 查询参选人信息详细
export function getCandidate(name) {
    return request({
        url: '/system/candidate/' + name,
        method: 'get'
    })
}

// 新增参选人信息
export function addCandidate(data) {
    return request({
        url: '/system/candidate',
        method: 'post',
        data: data
    })
}

// 修改参选人信息
export function updateCandidate(data) {
    return request({
        url: '/system/candidate',
        method: 'put',
        data: data
    })
}

// 删除参选人信息
export function delCandidate(name) {
    return request({
        url: '/system/candidate/' + name,
        method: 'delete'
    })
}

// 导出参选人信息
export function exportCandidate(query) {
    return request({
        url: '/system/candidate/export',
        method: 'get',
        params: query
    })
}
